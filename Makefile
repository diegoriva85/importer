.PHONY: up
up:
	docker-compose build
	docker-compose up -d
	docker exec importer-app ./bin/setup-docker.sh

.PHONY: bash
bash:
	docker exec -it importer-app /bin/bash

.PHONY: down
down:
	docker-compose down

.PHONY: clean
clean:
	docker-compose down -v

.PHONY: test_unit
test_unit:
	docker exec importer-app bin/phpunit tests/Unit

.PHONY: test_all
test_all:
	docker exec importer-app bin/phpunit tests
