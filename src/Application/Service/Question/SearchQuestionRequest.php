<?php

namespace App\ReportsImporter\Application\Service\Question;


class SearchQuestionRequest
{
    private $tagged;
    private $toDate;
    private $fromDate;

    public function __construct(
        string $tagged,
        string $toDate=null,
        string $fromDate=null
    ) {
        $this->tagged = $tagged;
        $date = date("Y-m-d");
        $this->toDate = empty($toDate) ?$date : $toDate;
        $this->fromDate = empty($fromDate) ? date("Y-m-d",strtotime($date."- 4 week"))  : $fromDate;
    }

    public function tagged(): string
    {
        return $this->tagged;
    }

    public function toDate(): string
    {
        return $this->toDate;
    }

    public function fromDate(): string
    {
        return $this->fromDate;
    }


}
