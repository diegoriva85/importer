<?php

namespace App\ReportsImporter\Infrastructure\Http\Controller;

use App\Tracking\Application\Tracking\CreateTracking;
use App\Tracking\Domain\Tracking\TrackingRepository;
use Symfony\Component\HttpFoundation\Response;

class SiteController
{
    public function index()
    {
        return new Response("Tracking Service is working");
    }
}
