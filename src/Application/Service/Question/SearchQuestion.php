<?php

namespace App\ReportsImporter\Application\Service\Question;

use App\ReportsImporter\Domain\Model\Question\QuestionReader;

class SearchQuestion
{
    private $questionReader;

    public function __construct(
        QuestionReader $questionReader
    ) {
        $this->questionReader = $questionReader;
    }

    public function execute(SearchQuestionRequest $request)
    {
        return $this->questionReader->execute($request);
    }
}
