<?php


namespace App\ReportsImporter\Infrastructure\Domain\Model\Question;


use App\ReportsImporter\Application\Service\Question\SearchQuestionRequest;
use App\ReportsImporter\Domain\Model\Question\QuestionReader;
use GuzzleHttp\Client;

class StackExchangeAPIManager implements QuestionReader
{
    private $guzzleClient;

    public function __construct()
    {
        $this->guzzleClient = new Client(['base_uri'=>"https://api.stackexchange.com"]);
    }

    public function execute(SearchQuestionRequest $request): array
    {
        $fromdate =strtotime($request->fromDate());
        $todate =strtotime($request->toDate());
        $tagged = $request->tagged();
        $response['SearchBy']['fromdate']=$request->fromDate();
        $response['SearchBy']['todate']=$request->toDate();
        $response['SearchBy']['tagged']=$tagged;

        $uri="/2.3/questions?fromdate={$fromdate}&todate={$todate}&order=desc&sort=activity&tagged={$tagged}&site=stackoverflow";
        $options = array(
            'headers' => array()
        );

        $responseApi = $this->guzzleClient->request('GET',$uri, $options);
        $responseContent = $responseApi->getBody()->getContents();
        $response['Result'] = json_decode($responseContent,true);


        return $response;
    }

}
