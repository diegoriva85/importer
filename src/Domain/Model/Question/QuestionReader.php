<?php


namespace App\ReportsImporter\Domain\Model\Question;


use App\ReportsImporter\Application\Service\Question\SearchQuestionRequest;

interface QuestionReader
{

    public function execute(SearchQuestionRequest $request): array;
}
