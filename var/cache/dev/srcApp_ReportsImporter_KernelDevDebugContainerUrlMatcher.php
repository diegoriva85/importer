<?php

use Symfony\Component\Routing\Matcher\Dumper\PhpMatcherTrait;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class srcApp_ReportsImporter_KernelDevDebugContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    use PhpMatcherTrait;

    public function __construct(RequestContext $context)
    {
        $this->context = $context;
        $this->staticRoutes = [
            '/' => [[['_route' => 'root', '_controller' => 'App\\ReportsImporter\\Infrastructure\\Http\\Controller\\SiteController::index'], null, null, null, false, false, null]],
            '/questions' => [[['_route' => 'api_search_questions', '_controller' => 'App\\ReportsImporter\\Infrastructure\\Http\\Controller\\QuestionController::search'], null, ['GET' => 0], null, false, false, null]],
        ];
        $this->regexpList = [
            0 => '{^(?'
                    .'|/questions(.*)(*:21)'
                .')/?$}sDu',
        ];
        $this->dynamicRoutes = [
            21 => [[['_route' => 'api_search_questions_all_routes', '_controller' => 'App\\ReportsImporter\\Infrastructure\\Http\\Controller\\QuestionController::options'], ['catchall'], ['OPTIONS' => 0], null, false, true, null]],
        ];
    }
}
