<?php

namespace App\ReportsImporter\Infrastructure\Http\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use App\ReportsImporter\Application\Service\Question\SearchQuestion;
use App\ReportsImporter\Application\Service\Question\SearchQuestionRequest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class QuestionController
{
    private $searchQuestionSrv;

    public function __construct(
        SearchQuestion $searchQuestionSrv
    ) {
        $this->searchQuestionSrv = $searchQuestionSrv;
    }

    public function search(Request $request)
    {
        return $this->callService(
            function () use ($request) {
                return $this->searchQuestionSrv->execute(new SearchQuestionRequest(
                    $request->get('tagged'),
                    $request->get('todate',null),
                    $request->get('fromdate',null)
                ));
            }
        );
    }

    protected function response($data, $status = 200)
    {
        return new JsonResponse(
            $data,
            $status,
            [
                'Access-Control-Allow-Origin' => '*',
                'Access-Control-Allow-Methods' => 'GET,POST,PATCH,PUT,OPTIONS,DELETE',
                'Content-type' => 'application/json',
                'Access-Control-Allow-Headers' => 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
            ]
        );
    }

    protected function callService(callable $exec)
    {
        try {
            $result = $exec();
            $response = [
                'status' => 'ok'
            ];
            if (isset($result)) {
                $response['data'] = $result;
            };
            return $this->response($response);
        } catch (\Exception $exception) {
            return $this->response([
                'status' => 'error',
                'message' => $exception->getMessage()
                //,'class' => get_class($exception)
            ]);
        }
    }

    public function options()
    {
        return $this->response(['status' => 'ok']);
    }

    protected function getInput(ArrayCollection $data, $key)
    {
        return (string)$data->containsKey($key) ? $data->get($key) : null;
    }

}
